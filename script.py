#import libraries
#%matplotlib inline
import re
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
dataset = pd.read_csv('train/train.tsv', sep='\t', header=None)
print(dataset.shape)
dataset.head()

X = dataset[4].values
Y1 = dataset[0].values
Y2 = dataset[1].values
#total number of values
n = len(X)
# mean of our inputs and outputs
Y = [];
for x in range(n):
	Y.append((((float)(Y1[x]))+((float)(Y2[x])))/2)
y_mean = np.mean(Y) + 25

X_m = []
for x in range(n):
	found = re.findall("1[7-9]{1}[0-9]{2}|20[0-1]{1}[0-9]{1}",X[x]);
	found_float = []
	for y in range(len(found)):
		found_float.append((float)(found[y]))
	if len(found) > 0:
		X_m.append(sum(found_float)/len(found))
	else:
		X_m.append(y_mean)
	
x_mean = np.mean(X_m)
print(x_mean)
print(y_mean)

# using the formula to calculate the b1 and b0
numerator = 0
denominator = 0
for i in range(n):
    numerator += (X_m[i] - x_mean) * (Y[i] - y_mean)
    denominator += (X_m[i] - x_mean) ** 2
    
b1 = numerator / denominator
b0 = y_mean - (b1 * x_mean)
#printing the coefficient
print(b1, b0)

valuesTest = []
with open("test-A/in.tsv") as infile:
    for line in infile:
        valuesTest.append(line)

predictions = []
for x in range(len(valuesTest)):
	found = re.findall("1[7-9]{1}[0-9]{2}|20[0-1]{1}[0-9]{1}",valuesTest[x]);
	found_float = []
	for y in range(len(found)):
		found_float.append((float)(found[y]))
	if len(found) > 0:
		strinn = b0 + b1 * sum(found_float)/len(found)
		predictions.append(strinn)
	else:
		predictions.append(y_mean)
np.savetxt('test-A/out.tsv', predictions, '%.0f')


valuesTest = []
with open("dev-0/in.tsv") as infile:
    for line in infile:
        valuesTest.append(line)

predictions = []
for x in range(len(valuesTest)):
	found = re.findall("1[7-9]{1}[0-9]{2}|20[0-1]{1}[0-9]{1}",valuesTest[x]);
	found_float = []
	for y in range(len(found)):
		found_float.append((float)(found[y]))
	if len(found) > 0:
		strinn = b0 + b1 * sum(found_float)/len(found)
		predictions.append(strinn)
	else:
		predictions.append(y_mean)
np.savetxt('dev-0/out.tsv', predictions, '%.0f')


valuesTest = []
with open("dev-1/in.tsv") as infile:
    for line in infile:
        valuesTest.append(line)

predictions = []
for x in range(len(valuesTest)):
	found = re.findall("1[7-9]{1}[0-9]{2}|20[0-1]{1}[0-9]{1}",valuesTest[x]);
	found_float = []
	for y in range(len(found)):
		found_float.append((float)(found[y]))
	if len(found) > 0:
		strinn = b0 + b1 * sum(found_float)/len(found)
		predictions.append(strinn)
	else:
		predictions.append(y_mean)
np.savetxt('dev-1/out.tsv', predictions, '%.0f')


#plotting values 
x_max = np.max(X_m) + 100
x_min = np.min(X_m) - 100
#calculating line values of x and y
x = np.linspace(x_min, x_max, 1000)
y = b0 + b1 * x
#plotting line 
plt.plot(x, y, color='#00ff00', label='Linear Regression')
#plot the data point
plt.scatter(X_m, Y, color='#ff0000', label='Data Point')
# x-axis label
plt.xlabel('Text mean')
#y-axis label
plt.ylabel('Year')
plt.legend()
plt.show()
